# cache-digest-koel 🐦

> *koel*
>
> n. A bird of the genus Eudynamys, cuckoos from Asia, Australia and the Pacific

![Koel in Singapore](./koel.jpg)

Prototype implementation in JavaScript of the Cache Digest HTTP/2 extension using Cuckoo Filters instead of Golomb Coded Sets (GCS) aka Bloom Filters. See the [proposed specification by Yoav Weiss](https://github.com/httpwg/http-extensions/pull/413).

The goal is to measure digest size. Not optimised for computational performance.

Developed at the [IETF 100 hackathon](https://www.ietf.org/registration/MeetingWiki/wiki/doku.php?id=100hackathon) in 🇸🇬 Singapore.

Thanks to [Kazuho](https://twitter.com/kazuho) for his guidance and patience.
