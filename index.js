const readBits = require('./readBits')
const writeBits = require('./writeBits')
const sha256 = require('./sha256')

module.exports.CacheDigest =
class CacheDigest {
  // ### Creating a digest {#creating}
  constructor
  // Given the following inputs:
  (
    // * `P`, an integer smaller than 256, that indicates the probability of a false positive that is acceptable, expressed as `1/2\*\*P`.
    P,
    // * `N`, an integer that represents the number of entries - a prime number smaller than 2\*\*32
    N
  ) {
    if (typeof P !== 'number') throw new Error('P must be an integer')
    if (P < 0) throw new Error('P must be greater than 0')
    if (P >= 256) throw new Error('P must be less than 256')
    if (typeof N !== 'number') throw new Error('N must be an integer')
    if (N < 0) throw new Error('N must be greater than 0')
    if (N >= 2 ** 32) throw new Error('N must be less than 2 ** 32')
    // 1. Let `f` be the number of bits per fingerprint, calculated as `P + 3`
    const f = P + 3
    // 2. Let `b` be the bucket size, defined as 4
    const b = 4
    // 3. Let `bytes` be `f`*`N`*`b`/8 rounded up to the nearest integer
    let bytes = Math.ceil(f * N * b / 8)
    // 4. Add 5 to `bytes`
    bytes += 5
    // 5. Allocate memory of `bytes` and set it to zero. Assign it to `digest-value`.
    const digestValue = new Uint8Array(bytes)
    // 6. Set the first byte to `P`
    digestValue[0] = P
    // 7. Set the second till fifth bytes to `N` in big endian form
    digestValue[1] = (N & 0xff000000) >> 24
    digestValue[2] = (N & 0x00ff0000) >> 16
    digestValue[3] = (N & 0x0000ff00) >> 8
    digestValue[4] = (N & 0x000000ff)
    // 8. Return the `digest-value`.
    this.digestValue = digestValue
  }

  // ### Adding a URL to the Digest-Value {#adding}
  add
  // Given the following inputs:
  (
    // * `URL` a string corresponding to the Effective Request URI ({{RFC7230}}, Section 5.5) of a cached response {{RFC7234}}
    URL,
    // * `ETag` a string corresponding to the entity-tag {{RFC7232}} if a cached response {{RFC7234}} (if the ETag is available; otherwise, null);
    ETag,
    // * `maxcount` - max number of cuckoo hops
    maxcount,
    // * `digest-value`
    digestValue
  ) {
    // 1. Let `f` be the value of the first byte of `digest-value`.
    const f = this.digestValue[0]
    // 2. Let `b` be the bucket size, defined as 4.
    const b = 4
    // 3. Let `N` be the value of the second to fifth bytes of `digest-value` in big endian form.
    const N =
      digestValue[1] << 24 |
      digestValue[2] << 16 |
      digestValue[3] << 8 |
      digestValue[4]
    // 4. Let `key` be the return value of {{key}} with `URL` and `ETag` as inputs.
    const key = this.key(URL, ETag)
    // 5. Let `h1` be the return value of {{hash}} with `key` and `N` as inputs.
    let h1 = this.hash(key, N)
    // 6. Let `fingerprint` be the return value of {{fingerprint}} with `key`, `N` and `f` as inputs.
    let fingerprint = this.fingerprint(key, N, f)

    do {
      // 7. Let `h2` be the return value of {{hash}} with `fingerprint` and `N` as inputs, XORed with `h1`.
      const h2 = this.hash(fingerprint, N) ^ h1
      // 8. Let `h` be either `h1` or `h2`, picked in random.
      const h = Math.random() < 0.5 ? h1 : h2
      // 9. Let `position_start` be 40 + `h` * `f`.
      let positionStart = 40 + h * f
      // 10. Let `position_end` be `position_start` + `f` * `b`.
      const positionEnd = positionStart + f * b
      // 11. While `position_start` < `position_end`:
      while (positionStart < positionEnd) {
        // 1. Let `bits` be `f` bits from `digest_value` starting at `position_start`.
        let bits = readBits(this.digestValue, positionStart, f)
        // 2. If `bits` is all zeros, set `bits` to `fingerprint` and terminate these steps.
        if (bits === 0) {
          writeBits(this.digestValue, positionStart, f, fingerprint)
          return
        }
        // 3. Add `f` to `position_start`.
        positionStart = positionStart + f
      }
      // 12. Substract `f` from `position_start`.
      positionStart = positionStart - f
      // 13. Let `fingerprint` be the `f` bits starting at `position_start`.
      fingerprint = readBits(digestValue, positionStart, f)
      // 14. Let `h1` be `h`
      h1 = h
      // 15. Substract 1 from `maxcount`.
      maxcount = maxcount - 1
      // 16. If `maxcount` is zero, return an error.
      if (maxcount === 0) {
        return new Error()
      }
      // 17. Go to step 7.
    } while (true)
  }

  // ### Removing a URL to the Digest-Value {#removing}
  remove
  // Given the following inputs:
  (
    // * `URL` a string corresponding to the Effective Request URI ({{RFC7230}}, Section 5.5) of a cached response {{RFC7234}}
    URL,
    // * `ETag` a string corresponding to the entity-tag {{RFC7232}} if a cached response {{RFC7234}} (if the ETag is available; otherwise, null);
    ETag,
    // * `digest-value`
    digestValue
  ) {
    // 1. Let `f` be the value of the first byte of `digest-value`.
    const f = this.digestValue[0]
    // 3. Let `N` be the value of the second to fifth bytes of `digest-value` in big endian form.
    const N =
      digestValue[1] << 24 |
      digestValue[2] << 16 |
      digestValue[3] << 8 |
      digestValue[4]
    // 4. Let `key` be the return value of {{key}} with `URL` and `ETag` as inputs.
    const key = this.key(URL, ETag)
    // 5. Let `h1` be the return value of {{hash}} with `key` and `N` as inputs.
    let h1 = this.hash(key, N)
    // 6. Let `fingerprint` be the return value of {{fingerprint}} with `key`, `N` and `f` as inputs.
    let fingerprint = this.fingerprint(key, N, f)
    // 7. Let `h2` be the return value of {{hash}} with `fingerprint` and `N` as inputs, XORed with `h1`.
    const h2 = this.hash(fingerprint, N) ^ h1
    // 8. Let `h` be `h1`.
    let h = h1
    do {
      // 9. Let `position_start` be 40 + `h` * `f`.
      let positionStart = 40 + h * f
      // 10. Let `position_end` be `position_start` + `f` * `b`.

      // @TODO(sebdeckers): 'b' is not defined
      const b = 4

      let positionEnd = positionStart + f * b
      // 11. While `position_start` < `position_end`:
      while (positionStart < positionEnd) {
        // 1. Let `bits` be `f` bits from `digest_value` starting at `position_start`.
        let bits = readBits(this.digestValue, positionStart, f)
        // 2. If `bits` is `fingerprint`, set `bits` to all zeros and terminate these steps.
        if (bits === fingerprint) {
          writeBits(this.digestValue, positionStart, f, 0)
          return
        }
        // 3. Add `f` to `position_start`.
        positionStart = positionStart + f
      }
      // 12. If `h` is not `h2`, set `h` to `h2` and return to step 9.
      h = h2
    } while (h !== h2)
  }

  // ### Computing a fingerprint value {#fingerprint}
  fingerprint
  // Given the following inputs:
  (
    // * `key`, an array of characters
    key,
    // * `N`, an integer
    N,
    // * `f`, an integer indicating the number of output bits
    f
  ) {
    // 1. Let `hash-value` be the return value of {{hash}} with `key` and `N` as inputs.
    let hashValue = this.hash(key, N)
    // 2. Let `h` be the number of bits in `hash-value`
    let h = Math.floor(Math.log2(hashValue))
    // 3. Let `fingerprint-value` be 0
    let fingerprintValue = 0
    // 4. While `fingerprint-value` is 0 and `h` > `f`:
    while (fingerprintValue === 0 && h > f) {
      // 4.1. Let `fingerprint-value` be the `f` least significant bits of `hash-value`.
      fingerprintValue = hashValue & (2 ** f - 1)
      // 4.2. Let `hash-value` be the the `h`-`f` most significant bits of `hash-value`.
      const mostSignificantBitsCount = h - f
      hashValue = hashValue >>> (32 - mostSignificantBitsCount) // @TODO(sebdeckers): Figure out this bit-trickery
      // 4.3. `h` -= `f`
      h -= f
    }
    // 5. If `fingerprint-value` is 0, let `fingerprint-value` be 1.
    if (fingerprintValue === 0) {
      fingerprintValue = 1
    }
    // 6. Return `fingerprint-value`.
    return fingerprintValue
    // Note: Step 5 is to handle the extremely unlikely case where a SHA-256 digest of `key` is all zeros. The implications of it means that
    // there's an infitisimaly larger probability of getting a `fingerprint-value` of 1 compared to all other values. This is not a problem for any
    // practical purpose.
  }

  // ### Computing the key {#key}
  key
  // Given the following inputs:
  (
    // * `URL`, an array of characters
    URL,
    // * `ETag`, an array of characters
    ETag
  ) {
    // 1. Let `key` be `URL` converted to an ASCII string by percent-encoding as appropriate {{RFC3986}}.
    let key = encodeURIComponent(URL)
    // 2. If `ETag` is not null:
    if (ETag !== null) {
      // 1. Append `ETag` to `key` as an ASCII string, including both the `weak` indicator (if present) and double quotes, as per {{RFC7232}}, Section 2.3.
      // @TODO(sebdeckers): How does this work?
      key = key + ETag
    }
    // 3. Return `key`
    return key
  }

  // ### Computing a Hash Value {#hash}
  hash
  // Given the following inputs:
  (
    // * `key`, an array of characters.
    key,
    // * `N`, an integer
    N
  ) {
    // `hash-value` can be computed using the following algorithm:
    // 1. Let `hash-value` be the SHA-256 message digest {{RFC6234}} of `key`, expressed as an integer.
    const hashValue = sha256(key)
    // 2. Return `hash-value` modulo N.
    // @TODO(sebdeckers): Is this correct/possible in JS without BigInt?
    return hashValue % N
  }

  // ### Querying the Digest for a Value {#querying}
  query
  // Given the following inputs:
  (
    // * `URL` a string corresponding to the Effective Request URI ({{RFC7230}}, Section 5.5) of a cached response {{RFC7234}}.
    URL,
    // * `ETag` a string corresponding to the entity-tag {{RFC7232}} if a cached response {{RFC7234}} (if the ETag is available; otherwise, null).
    ETag,
    // * `digest-value`, an array of bits.
    digestValue
  ) {
    // 1. Let `f` be the value of the first byte of `digest-value`.
    const f = this.digestValue[0]
    // 3. Let `N` be the value of the second to fifth bytes of `digest-value` in big endian form.
    const N =
      digestValue[1] << 24 |
      digestValue[2] << 16 |
      digestValue[3] << 8 |
      digestValue[4]
    // 4. Let `key` be the return value of {{key}} with `URL` and `ETag` as inputs.
    const key = this.key(URL, ETag)
    // 5. Let `h1` be the return value of {{hash}} with `key` and `N` as inputs.
    const h1 = this.hash(key, N)
    // 6. Let `fingerprint` be the return value of {{fingerprint}} with `key`, `N` and `f` as inputs.
    const fingerprint = this.fingerprint(key, N, f)
    // 7. Let `h2` be the return value of {{hash}} with `fingerprint` and `N` as inputs, XORed with `h1`.
    this.h2 = this.hash(fingerprint, N) ^ h1
    // 8. Let `h` be `h1`.
    const h = h1
    // 9. Let `position_start` be 40 + `h` * `f`.
    let positionStart = 40 * h * f

    // @TODO(sebdeckers): 'b' is not defined
    const b = 4

    // 10. Let `position_end` be `position_start` + `f` * `b`.
    const positionEnd = positionStart + f * b
    // 11. While `position_start` < `position_end`:
    while (positionStart < positionEnd) {
      // 1. Let `bits` be `f` bits from `digest_value` starting at `position_start`.
      const bits = readBits(this.digestValue, positionStart, f)
      // 2. If `bits` is `fingerprint`, return true
      if (bits === fingerprint) {
        return true
      }
      // 3. Add `f` to `position_start`.
      positionStart = positionStart + f
    }
    // 12. Return false.
    return false
  }
}
