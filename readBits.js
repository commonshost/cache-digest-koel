module.exports.readBits =
function readBits (bytes, start, length) {
  const view = new DataView(bytes.buffer)
  // console.log(bytes)

  const dwordOffset = Math.floor(start / 32)
  const secondDwordStart = 32 * (1 + dwordOffset)
  const firstDword = view.getUint32(dwordOffset * 4)
  const secondDword = view.getUint32((dwordOffset + 1) * 4)
  // console.log({
  //   dwordOffset,
  //   firstDword: firstDword.toString(16),
  //   secondDword: secondDword.toString(16)
  // })

  const headBegin = start % 32
  const headEnd = Math.min(headBegin + length, 32)
  // const headLength = headEnd - headBegin
  // console.log({headBegin, headEnd, headLength})

  const fullBits = 0xffffffff

  const headMaskBefore = (fullBits >>> headBegin) >>> 0
  const headMaskAfter = (fullBits << (32 - headEnd)) >>> 0
  // console.log({
  //   headMaskBefore: headMaskBefore.toString(2),
  //   howManyBefore: headMaskBefore.toString(2).length,
  //   headMaskAfter: headMaskAfter.toString(2),
  //   howManyAfter: headMaskAfter.toString(2).length
  // })

  const headMasked = headMaskBefore & firstDword & headMaskAfter
  const headTrimmed = headMasked >>> (32 - headEnd)
  // console.log({
  //   headMasked: headMasked.toString(16),
  //   headTrimmed: headTrimmed.toString(16)
  // })

  if (start + length <= secondDwordStart) {
    return headTrimmed
  }

  const tailBegin = 0
  const tailEnd = Math.max(0, start + length - secondDwordStart)
  const tailLength = tailEnd - tailBegin
  // console.log({tailBegin, tailEnd, tailLength})

  // const tailMaskBefore = (fullBits >>> tailBegin) >>> 0
  const tailMaskAfter = (fullBits << (32 - tailEnd)) >>> 0
  // console.log({
  //   // tailMaskBefore: tailMaskBefore.toString(2),
  //   // howManyBefore: tailMaskBefore.toString(2).length,
  //   tailMaskAfter: tailMaskAfter.toString(2),
  //   howManyAfter: tailMaskAfter.toString(2).length
  // })

  const tailMasked = (secondDword & tailMaskAfter) >>> 0
  const tailTrimmed = tailMasked >>> (32 - tailEnd)
  // console.log({
  //   tailMasked: tailMasked.toString(16),
  //   tailTrimmed: tailTrimmed.toString(16)
  // })

  // console.log({
  //   headTrimmed: headTrimmed.toString(2),
  //   tailTrimmed: tailTrimmed.toString(2)
  // })

  return ((headTrimmed << tailLength) | tailTrimmed) >>> 0
}
