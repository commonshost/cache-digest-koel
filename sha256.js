const crypto = require('crypto')

module.exports.sha256 =
function sha256 (message) {
  return crypto.createHash('sha256').update(message).digest()
}
