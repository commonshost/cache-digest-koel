const test = require('ava')
const {readBits} = require('../readBits')

const allOnes = [
  0xff, 0xff, 0xff, 0xff,
  0xff, 0xff, 0xff, 0xff,
  0xff, 0xff, 0xff, 0xff,
  0xff, 0xff, 0xff, 0xff,
]

const allZeros = [
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00
]

const gradient = [
  0x11, 0x22, 0x33, 0x44,
  0x55, 0x66, 0x77, 0x88,
  0x99, 0xaa, 0xbb, 0xcc,
  0xdd, 0xee, 0xff, 0x00
]

const alternating = [
  0xf0, 0xf0, 0xf0, 0xf0,
  0xf0, 0xf0, 0xf0, 0xf0,
  0xf0, 0xf0, 0xf0, 0xf0,
  0xf0, 0xf0, 0xf0, 0xf0
]

const fixtures = [
  {
    title: 'First full dword',
    bytes: allOnes,
    start: 0,
    length: 32,
    expected: 2 ** 32 - 1
  },
  {
    title: 'Overlapping strip of full bits',
    bytes: allOnes,
    start: 20,
    length: 20,
    expected: 2 ** 20 - 1
  },
  {
    title: 'Part of second dword',
    bytes: allOnes,
    start: 40,
    length: 10,
    expected: 2 ** 10 - 1
  },
  {
    title: 'First gradient dword',
    bytes: gradient,
    start: 0,
    length: 32,
    expected: 0x11223344
  },
  {
    title: 'Strip of gradient',
    bytes: gradient,
    start: 24,
    length: 24,
    expected: 0x445566
  },
  {
    title: 'Read zero',
    bytes: allZeros,
    start: 31,
    length: 2,
    expected: 0
  },
  {
    title: 'Read alternating',
    bytes: alternating,
    start: 22,
    length: 22,
    expected: 0b0011110000111100001111
  }
]

for (const {title, bytes, start, length, expected} of fixtures) {
  test(title, (t) => {
    const given = new Uint8Array(bytes)
    const actual = readBits(given, start, length)
    t.is(actual.toString(2), expected.toString(2))
  })
}
