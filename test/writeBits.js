const test = require('ava')
const {writeBits} = require('../writeBits')

const allZeros = [
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00
]

const fixtures = [
  {
    title: 'Empty byte array',
    bytes: allZeros,
    start: 0,
    length: 32,
    expected: [
      0x00, 0x00, 0x0f, 0xff,
      0xf0, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00
    ]
  }
]

for (const {title, bytes, start, length, expected} of fixtures) {
  test(title, (t) => {
    const given = new Uint8Array(bytes)
    const actual = writeBits(given, start, length)
    t.is(actual.toString(2), expected.toString(2))
  })
}
