module.exports.writeBits =
function writeBits (bytes, start, length, value) {
  const view = new DataView(bytes.buffer)

  const headLength = Math.min(length, 32 - headBegin)
  const head = (value >>> headLength) & 0xffffffff

  view.setUint32()

  const dwordOffset = Math.floor(start / 32)

  return bytes
}

// 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32

// 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16
// 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32
